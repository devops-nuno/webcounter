import sys
import os

## Add root path to allow run file form inside and outside path
sys.path.insert(0, os.path.join(os.getcwd(), "webcounter"))

print(sys.path)
# import redis cache
from webcounter import get_hits_count

def test_get_hits_count():
    """ Just test get_hits_count """
    
    start_counter = get_hits_count()
    result = get_hits_count()
    assert(result > start_counter)